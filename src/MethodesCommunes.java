

import java.awt.AWTException;
import java.awt.Color;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class MethodesCommunes {

	//get html from url
	public static String GetHTMLFromURL(String url) {
		
		Document doc = null;
		String html = "";
		
		try
		{
			
			//on se connecte � la page
			doc = Jsoup.connect(url).userAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)").get();
			
			//on r�cup�re la page html
			html = doc.html().replace("\n", "").replace("\r", "").replace("\t", "").trim();
			//html = doc.html(); System.out.println(html);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
		return html;
	}
	
	//write file
	public static void WriteFile(String path, String content) {
		
		BufferedWriter bw = null;
		FileWriter fw = null;
		
		try
		{
			
			fw = new FileWriter(path);
			bw = new BufferedWriter(fw);
			bw.write(content);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		
		} 
		finally 
		{
			try 
			{

				if (bw != null) {bw.close();}
				if (fw != null) {fw.close();}

			} catch (IOException ex) {ex.printStackTrace();}
		}
	}
	
	//read file
	public static ArrayList<String> ReadFile(String pathFile) {
		
		BufferedReader br = null;
		FileReader fr = null;
		
		ArrayList<String> fileContent = new ArrayList<String>();

		String sCurrentLine;
		
		try
		{
			
			fr = new FileReader(pathFile);
			br = new BufferedReader(fr);

			br = new BufferedReader(new FileReader(pathFile));

			while ((sCurrentLine = br.readLine()) != null) {

				fileContent.add(sCurrentLine);
				
			}
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		
		} 
		finally 
		{
			try 
			{

				if (br != null) {br.close();}
				if (fr != null) {fr.close();}

			} catch (IOException ex) {ex.printStackTrace();}
		}
		
		return fileContent;
		
	}
	
	//read all files in folder
	public static ArrayList<String> ReadAllFilesInFolder(String pathFolder) {
		
		File folder = null;
		File[] listOfFiles = null;
		
		ArrayList<String> AllfilesContent = new ArrayList<String>();
		ArrayList<String> fileContent = new ArrayList<String>();
		
		try
		{
			
			//on r�cup�re la liste des files du folder
			folder = new File(pathFolder);
			listOfFiles = folder.listFiles();
			
			for (int i=0;i<listOfFiles.length;i++) {
				
				System.out.println(i);
				
				fileContent = ReadFile(pathFolder + "\\" + listOfFiles[i].getName());
				AllfilesContent.addAll(fileContent);
				
			}
		}
		catch (Exception e) {
			
			e.printStackTrace();
		
		} 
		
		return AllfilesContent;
		
	}	
	
	//copy/paste clipboard
	public static void CopyInClipboard(String textToCopy) throws AWTException, URISyntaxException, IOException {
		
		try
		{
			
			StringSelection stringSelection = new StringSelection(textToCopy);
			Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
			clpbrd.setContents(stringSelection, null);
		
		}
		catch (Exception e) {
			
			e.printStackTrace();
		
		}		
	}
	
	public static String getClipboardContent() {
	/**
	 * return any text found on the Clipboard; if none found, return an empty String.
	 */
		String result = "";
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    
	    try
		{
	    	
		    Transferable contents = clipboard.getContents(null);
		    boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		    
		    if (hasTransferableText) {
		    
		    	try 
		    	{
		    		result = (String)contents.getTransferData(DataFlavor.stringFlavor);
		    	}
		    	catch (Exception e){
		    		e.printStackTrace();
		    	}
		    }
		}
	    catch (Exception e) {
			
			e.printStackTrace();
		
		}
	    
	    return result;
	
	}
	
	//read coordinates on screen
	public static void ReadCoordinatesOnScreen(int minutes) throws AWTException {
		
		Robot robot = new Robot();
		
		int currentMinute = 0;
		int currentSeconde = 0;
		
		try
		{
			
			System.out.println("--------------------------EXTRAC MOUSE POSITION START-------------------------------");
			
			while (currentMinute < minutes) {
			
				//on initialise le nombre de secondes
				currentSeconde = 0;
				
				//tant que la minute n'est pas fini
				while (currentSeconde < 60) {
					
					Color color = robot.getPixelColor((int) MouseInfo.getPointerInfo().getLocation().getX(), (int) MouseInfo.getPointerInfo().getLocation().getY());
					
					//on affiche les coordonn�es du mouse
					System.out.println(MouseInfo.getPointerInfo().getLocation() + " - " + color.getRed() + "," + color.getGreen() + "," + color.getBlue());
					
					//on attend 5 secondes
					Thread.sleep(5000);
					
					//on ajoute 5 secondes
					currentSeconde = currentSeconde + 5;
				}
				
				//on incr�mente le nombre de minutes
				currentMinute++;
				
			}		
			
			System.out.println("--------------------------EXTRAC MOUSE POSITION FINISH-------------------------------");
			
		}
		catch (Exception e) {
				
			e.printStackTrace();
			
		}
	}
	
	//get pixel color on screen
	public static void GetPixelColorOnScreen(int minutes) throws AWTException {
		
		Robot robot = new Robot();
		
		int currentMinute = 0;
		int currentSeconde = 0;
		
		try
		{
			
			System.out.println("--------------------------EXTRAC MOUSE POSITION START-------------------------------");
			
			while (currentMinute < minutes) {
			
				//on initialise le nombre de secondes
				currentSeconde = 0;
				
				//tant que la minute n'est pas fini
				while (currentSeconde < 60) {
					
					Color color = robot.getPixelColor((int) MouseInfo.getPointerInfo().getLocation().getX(), (int) MouseInfo.getPointerInfo().getLocation().getY());
					
					//on affiche la couleur
					System.out.println(color.getRed() + "," + color.getGreen() + "," + color.getBlue());
					
					//on attend 5 secondes
					Thread.sleep(5000);
					
					//on ajoute 5 secondes
					currentSeconde = currentSeconde + 5;
				}
				
				//on incr�mente le nombre de minutes
				currentMinute++;
				
			}		
			
			System.out.println("--------------------------EXTRAC MOUSE POSITION FINISH-------------------------------");
			
		}
		catch (Exception e) {
				
			e.printStackTrace();
			
		}
	}
	
	//robots
	public static void RightClickOpenInNewTab() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un click droit
			 robot.keyPress(KeyEvent.VK_SHIFT);
			 robot.keyPress(KeyEvent.VK_F10);
			 robot.keyRelease(KeyEvent.VK_F10);
			 robot.keyRelease(KeyEvent.VK_SHIFT);
			 
			 robot.delay(500);
			 
			 //on appuie sur la fleche du bas pour se placer sur ouvrir dans un nouvel onglet
			 robot.keyPress(KeyEvent.VK_DOWN);
			 robot.keyRelease(KeyEvent.VK_DOWN);
			 
			 robot.delay(500);
			 
			 //on appuie sur entr�e
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
			
		}
	}
	
	public static void RightClickCopyURL() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un click droit
			 robot.keyPress(KeyEvent.VK_SHIFT);
			 robot.keyPress(KeyEvent.VK_F10);
			 robot.keyRelease(KeyEvent.VK_F10);
			 robot.keyRelease(KeyEvent.VK_SHIFT);
			 
			 robot.delay(500);
			 
			 //on appuie sur la fleche du bas 5 fois pour se placer sur ouvrir dans un nouvel onglet
			 for (int i=0;i<5;i++) {
				 
				 robot.keyPress(KeyEvent.VK_DOWN);
				 robot.keyRelease(KeyEvent.VK_DOWN);
				 
			 }
			 
			 robot.delay(500);
			 
			 //on appuie sur entr�e
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
			
		}
	}
	
	public static void OpenOrCloseConsole() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un tab
			 robot.keyPress(KeyEvent.VK_CONTROL);
			 robot.keyPress(KeyEvent.VK_SHIFT);
			 robot.keyPress(KeyEvent.VK_J);
			 robot.keyRelease(KeyEvent.VK_CONTROL);
			 robot.keyRelease(KeyEvent.VK_SHIFT);
			 robot.keyRelease(KeyEvent.VK_J);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void GoToNextTab() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un click droit
			 robot.keyPress(KeyEvent.VK_CONTROL);
			 robot.keyPress(KeyEvent.VK_TAB);
			 robot.keyRelease(KeyEvent.VK_TAB);
			 robot.keyRelease(KeyEvent.VK_CONTROL);
			 			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	public static void AltTab() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un alt tab
			 robot.keyPress(KeyEvent.VK_ALT);
			 robot.keyPress(KeyEvent.VK_ESCAPE);
			 robot.keyRelease(KeyEvent.VK_ESCAPE);
			 robot.keyRelease(KeyEvent.VK_ALT);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void Copy() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un ctrl + c
			 robot.keyPress(KeyEvent.VK_CONTROL);
			 robot.keyPress(KeyEvent.VK_C);
			 robot.keyRelease(KeyEvent.VK_C);
			 robot.keyRelease(KeyEvent.VK_CONTROL);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void Paste() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un ctrl + v
			 robot.keyPress(KeyEvent.VK_CONTROL);
			 robot.keyPress(KeyEvent.VK_V);
			 robot.keyRelease(KeyEvent.VK_V);
			 robot.keyRelease(KeyEvent.VK_CONTROL);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void Save() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un ctrl + s
			 robot.keyPress(KeyEvent.VK_CONTROL);
			 robot.keyPress(KeyEvent.VK_S);
			 robot.keyRelease(KeyEvent.VK_S);
			 robot.keyRelease(KeyEvent.VK_CONTROL);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void Enter() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un enter
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void CtrlEnter() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un ctrl + enter
			 robot.keyPress(KeyEvent.VK_CONTROL);
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_CONTROL);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void CtrlA() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un ctrl + enter
			 robot.keyPress(KeyEvent.VK_CONTROL);
			 robot.keyPress(KeyEvent.VK_A);
			 robot.keyRelease(KeyEvent.VK_A);
			 robot.keyRelease(KeyEvent.VK_CONTROL);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	public static void Tab() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un tab
			 robot.keyPress(KeyEvent.VK_TAB);
			 robot.keyRelease(KeyEvent.VK_TAB);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void TabBack() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			//on simule un shit + tab
			robot.keyPress(KeyEvent.VK_SHIFT); 
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_SHIFT);
		
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}	
	
	public static void CloseTab() throws AWTException{
		
		Robot robot = new Robot();
		
		try
		{
			
			//on simule un ctrl + w
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_W);
			robot.keyRelease(KeyEvent.VK_W);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	public static void Down() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 //on simule un click sur la fleche pour descendre
			 robot.keyPress(KeyEvent.VK_DOWN);
			 robot.keyRelease(KeyEvent.VK_DOWN);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void FocusOnUrl() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 robot.keyPress(KeyEvent.VK_F6);
			 robot.keyRelease(KeyEvent.VK_F6);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void Escape() throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			 robot.keyPress(KeyEvent.VK_ESCAPE);
			 robot.keyRelease(KeyEvent.VK_ESCAPE);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void MoveMouseAndClick(int x, int y) throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			robot.mouseMove(x,y); 
			Thread.sleep(1000); 
			robot.mousePress(InputEvent.BUTTON1_MASK); 
			robot.mouseRelease(InputEvent.BUTTON1_MASK);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static void MoveMouseAndRightClick(int x, int y) throws AWTException {
		
		Robot robot = new Robot();
		
		try
		{
			
			robot.mouseMove(x,y); 
			Thread.sleep(1000); 
			robot.mousePress(InputEvent.BUTTON3_MASK); 
			robot.mouseRelease(InputEvent.BUTTON3_MASK);
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
