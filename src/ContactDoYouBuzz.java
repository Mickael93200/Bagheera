
public class ContactDoYouBuzz {
	
	private String Url = "";	
	private String Firstname = "";
	private String Lastname = "";
	private String Title = "";
	private String Age = "";
	private String Location = "";
	private String Status = "";
	private String Availability = "";
	private String Linkedin = "";
	private String Company = "";
	private String Email = "";
	private String Phone = "";
	private String Mobile = "";
	private String Skype = "";
			
	public ContactDoYouBuzz() {
		
	}
			
	public ContactDoYouBuzz(String url, String firstname, String lastname, String title, String age, String location, String status, String availability, String linkedin,
							String company, String email, String phone, String mobile, String skype) {	
		setUrl(url);
		setFirstname(firstname);
		setLastname(lastname);
		setTitle(title);
		setAge(age);
		setLocation(location);
		setStatus(status);
		setAvailability(availability);
		setLinkedin(linkedin);
		setCompany(company);
		setEmail(email);
		setPhone(phone);
		setMobile(mobile);
		setSkype(skype);		
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getAvailability() {
		return Availability;
	}

	public void setAvailability(String availability) {
		Availability = availability;
	}

	public String getLinkedin() {
		return Linkedin;
	}

	public void setLinkedin(String linkedin) {
		Linkedin = linkedin;
	}

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getSkype() {
		return Skype;
	}

	public void setSkype(String skype) {
		Skype = skype;
	}

	public String getLastname() {
		return Lastname;
	}

	public void setLastname(String lastname) {
		Lastname = lastname;
	}

	public String getFirstname() {
		return Firstname;
	}

	public void setFirstname(String firstname) {
		Firstname = firstname;
	}
}
