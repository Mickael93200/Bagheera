import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Robot;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Main {

	static String ResultPath = "C:\\Users\\Mickael\\Desktop\\";
	
	static String KeywordsInTitle = "cv";
	static String Keywords = "scrum master";
	static int ResultsPageNumber = 1;
	
	//voir les deux problemes
	//r�cup avec duckduckgo
	//syst�me pour �viter les doublons
	
	public static void main(String[] args) throws InterruptedException, AWTException {
		
		ExtractDoYouBuzz(KeywordsInTitle, Keywords, ResultsPageNumber);
		
		//Test();
		
	}
	
	/**
	 * Cette m�thode est utilis�e pour faire des tests rapides
	 */
	public static void Test() throws AWTException {
		
		HashSet<String> urlsToExploit = new HashSet<String>();
		//urlsToExploit.add("https://www.doyoubuzz.com/raimondi-benjamin");// profile normal
		urlsToExploit.add("https://www.doyoubuzz.com/eric-richard"); // aucune infos, pb des inners
		//urlsToExploit.add("https://www.doyoubuzz.com/julio-rosales"); // cas des tels anonimis�es
		
		ArrayList<ContactDoYouBuzz> contactsDoYouBuzz = new ArrayList<ContactDoYouBuzz>();
		
		for (String url : urlsToExploit) {
			
			ContactDoYouBuzz contact = ExtractionInformationProfile(url);
			
			if (!contact.getFirstname().isEmpty()) {
				contactsDoYouBuzz.add(contact);
			}
			
		}	

		WriteContactsToExcel(contactsDoYouBuzz);
	}
	
	public static void ExtractDoYouBuzz(String keywordsInTitle, String keywords, int numPages) throws InterruptedException, AWTException {
		
		String html, name = "";
		HashSet<String> urlsToExploit = new HashSet<String>(); 
		
		ArrayList<ContactDoYouBuzz> contactsDoYouBuzz = new ArrayList<ContactDoYouBuzz>();
		
		System.out.println("---------------------- EXTRACT DO YOU BUZZ ----------------------");	
		System.out.println("1) R�cup�ration des profils qui apparaissent dans Google ");
		System.out.println("	=> REQUETE: https://www.google.fr/search?q=site:doyoubuzz.com intitle:" + keywordsInTitle + " " + keywords);
		System.out.print("	=> Pages trait�es: ");
		
		for (int i=0;i<numPages;i++) {
			
			//on r�cup�re la page html
			html = MethodesCommunes.GetHTMLFromURL("https://www.google.fr/search?q=site:doyoubuzz.com intitle:" + keywordsInTitle + " " + keywords + "&start=" + i*10);

			//si la recherche ne contient pas de site, on sort de la boucle
			if (!html.contains("class=\"ZINbbc xpd O9g5cc uUPGi\"")) { break; }
			
			//on r�cup�re les infos
			while (html.contains("class=\"ZINbbc xpd O9g5cc uUPGi\"")) {
				
				html = html.substring(html.indexOf("class=\"ZINbbc xpd O9g5cc uUPGi\"") + 10);
				
				//on initialise les variables
				name = "";
				
				//on r�cup�re le nom dans l'url
				name = html.substring(html.indexOf("www.doyoubuzz.com/") + 18);
				name = name.substring(0,  name.indexOf("&"));
				if (name.contains("/")) { 
					name = name.substring(0,  name.indexOf("/")); 
				}
								
				//on ajoute l�url doYouBuzz de la personne en v�rifiant que le format est correct
				if (!(name.contains(" ") || name.contains("<") || name.contains(">") || name.contains("\""))) {
					urlsToExploit.add("https://www.doyoubuzz.com/" + name);
				}
			}
			
			System.out.print((i+1) + ".."); Thread.sleep(2000);
		}
		
		System.out.println(""); 
		System.out.println("	=> Nombre de profils uniques r�cup�r�s: " + urlsToExploit.size());
		System.out.println("");
		System.out.println("2) Extraction des informations sur les profiles r�cup�r�s");
			
		int count = 1;
		
		for (String url : urlsToExploit) {
			
			System.out.print("	" + count++ + "/" + urlsToExploit.size() + ": " + url); 
			
			ContactDoYouBuzz contact = ExtractionInformationProfile(url);
			
			System.out.print(" => done"); System.out.println("");
			
			if (!contact.getFirstname().isEmpty()) {
				contactsDoYouBuzz.add(contact);
			}
			
		}	
		
		//cr�ation de l�excel
		System.out.println("");
		System.out.println("3) Cr�ation de l�excel");
		
		WriteContactsToExcel(contactsDoYouBuzz);
		
		System.out.println("---------------------- FIN EXTRACT DO YOU BUZZ ----------------------");
	}
	
	public static ContactDoYouBuzz ExtractionInformationProfile(String url) throws AWTException  {
	
		String html, firstname = "", lastname = "", title = "", age = "", location = "", status = "", availability = "", linkedin = "", company = ""; 
		String email = "", phone = "", mobile = "", skype = "";
		Robot robot = new Robot();
		
		try
		{
			// PREMIERE PARTIE: on commence par r�cup�rer les infos accessible facilement
			
			//on r�cup�re la page html
			html = MethodesCommunes.GetHTMLFromURL(url);

			//on r�cup�re le firstname
			if (html.contains("userName__firstName")) {
				firstname = html.substring(html.indexOf("userName__firstName")); 
				firstname = firstname.substring(firstname.indexOf(">") + 1);
				firstname = firstname.substring(0, firstname.indexOf("<"));
			}
			
			//on r�cup�re le lastname
			if (html.contains("userName__lastName")) {
				lastname = html.substring(html.indexOf("userName__lastName")); 
				lastname = lastname.substring(lastname.indexOf(">") + 1);
				lastname = lastname.substring(0, lastname.indexOf("<"));
			}
			
			//on r�cup�re le title
			if (html.contains("cvTitle")) {
				title = html.substring(html.indexOf("cvTitle") + 9); 
				title = title.substring(0, title.indexOf("<")).replace("&amp;", "&");
			}
							
			//on r�cup�re l'�ge
			if (html.contains("item widgetUserInfo__item_age")) {
				age = html.substring(html.indexOf("item widgetUserInfo__item_age")); 
				age = age.substring(age.indexOf(">") + 1);
				age = age.substring(0, age.indexOf("<")).replace("years old", "").trim();
			}
							
			//on r�cup�re la ville
			if (html.contains("item widgetUserInfo__item_location")) {
				location = html.substring(html.indexOf("item widgetUserInfo__item_location")); 
				location = location.substring(location.indexOf(">") + 1);
				location = location.substring(0, location.indexOf("<")).trim();
			}
							
			//on r�cup�re le status
			if (html.contains("widgetProfessionalPosition__status")) {
				status = html.substring(html.indexOf("widgetProfessionalPosition__status")); 
				status = status.substring(status.indexOf(">") + 1);
				status = status.substring(0, status.indexOf("<")).trim();
			}
							
			//on r�cup�re la disponibilit�
			if (html.contains("widgetProfessionalPosition__availability")) {
				availability = html.substring(html.indexOf("widgetProfessionalPosition__availability")); 
				availability = availability.substring(availability.indexOf(">") + 1);
				availability = availability.substring(0, availability.indexOf("<")).trim();
			}
							
			//on r�cup�re le linkedin
			if (html.contains("widgetLinks__item_linkedin")) {
				linkedin = html.substring(html.indexOf("widgetLinks__item_linkedin")); 
				linkedin = linkedin.substring(linkedin.indexOf("href=") + 6);
				linkedin = linkedin.substring(0, linkedin.indexOf("\"")).trim();
			}				
			
			//on r�cup�re la company (la current uniquement)
			if (html.contains("widgetElement__subtitleItem_company")) {
				company = html.substring(html.indexOf("widgetElement__subtitleItem_company")); 
				company = company.substring(company.indexOf(">") + 1);
				company = company.substring(0, company.indexOf("<")).trim();
			}
							
//			System.out.println(firstname + " - " + lastname + " - " + title + " - " + company + " - " + age + " - " + location + " - " 
//							 + status + " - " + availability + " - " + linkedin);

			if (!firstname.isEmpty()) {
				
				//DEUXIEME PARTIE: on r�cup�re les infos de contact avec le bot
				
				//on ouvre la page
				URI uri = new URI(url);
			    Desktop dt = Desktop.getDesktop();
			    dt.browse(uri);
			    
			    robot.delay(5000);
			    
			    //on ouvre la console
			    MethodesCommunes.OpenOrCloseConsole();
			    
			    robot.delay(1000);
			    
			    //on clique sur le bouton contactez-moi
				MethodesCommunes.CopyInClipboard("var element = document.getElementsByClassName('contactsBox__openerLink')[0]; element.click();");
		    	MethodesCommunes.Paste(); robot.delay(500);
			    MethodesCommunes.Enter();
			    
			    robot.delay(2000);
			    
			    //on r�cup�re les infos disponibles
			    MethodesCommunes.CopyInClipboard("var email = document.getElementsByClassName('widgetContacts__item_publicemail')[0];\r\n" + 
									    		 "var phone = document.getElementsByClassName('widgetContacts__item_phone')[0];\r\n" + 
									    		 "var mobile = document.getElementsByClassName('widgetContacts__item_mobile')[0];\r\n" + 
									    		 "var skype = document.getElementsByClassName('widgetContacts__item_skype')[0];\r\n" +  
									    		 "var infos = \"\";\r\n" + 
									    		 "if (typeof email !== 'undefined') { infos += \"email:\" + email.innerHTML + \";\"; }\r\n" + 
									    		 "if (typeof phone !== 'undefined') { infos += \"phone:\" + phone.innerHTML + \";\"; }\r\n" + 
									    		 "if (typeof mobile !== 'undefined') { infos += \"mobile:\" + mobile.innerHTML + \";\"; }\r\n" + 
									    		 "if (typeof skype !== 'undefined') { infos += \"skype:\" + skype.innerHTML + \";\"; }\r\n" + 
									    		 "var inputElement = document.createElement('input'); inputElement.setAttribute('value', infos); document.body.appendChild(inputElement);\r\n" + 
									    		 "inputElement.select(); document.execCommand('copy'); document.body.removeChild(inputElement);");
			    MethodesCommunes.Paste(); robot.delay(500);
			    MethodesCommunes.Enter(); robot.delay(500);
			    String result = MethodesCommunes.getClipboardContent();
			    
			    if (result.contains("email:")) { email = result.substring(result.indexOf("email:") + 6); email = email.substring(0, email.indexOf(";"));}
			    if (result.contains("phone:")) { phone = result.substring(result.indexOf("phone:") + 6); phone = phone.substring(0, phone.indexOf(";"));}
			    if (result.contains("mobile:")) { mobile = result.substring(result.indexOf("mobile:") + 7); mobile = mobile.substring(0, mobile.indexOf(";"));}
			    if (result.contains("skype:")) { skype = result.substring(result.indexOf("skype:") + 6); skype = skype.substring(0, skype.indexOf(";"));}
			    
			    //pour les probl�me des anonymized & inner - TEMPORAIRE
			    if (email.contains("anonymized") || email.contains("innerHTML")) { email = ""; }
			    if (phone.contains("anonymized") || phone.contains("innerHTML")) { phone = ""; }
			    if (mobile.contains("anonymized") || mobile.contains("innerHTML")) { mobile = ""; }
			    if (skype.contains("anonymized") || skype.contains("innerHTML")) { skype = ""; }
			    
//			    System.out.println(email + " - " + phone + " - " + mobile + " - " + skype);
	
			    //on ferme l'onglet
			    MethodesCommunes.CloseTab();
			}
		}
		catch (Exception e) {
			
			System.out.println("ERREUR - url :" + url);
			e.printStackTrace();
		
		}
		
		return new ContactDoYouBuzz(url, firstname, lastname, title, age, location, status, availability, linkedin, company, email, phone, mobile, skype);
	}

	public static void WriteContactsToExcel(ArrayList<ContactDoYouBuzz> contactsDoYouBuzz) {
		
		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Extract-DoYouBuzz");
        
        Map<String, CellStyle> styles = createStyles(workbook);
         
        int rowCount = 0;

        //cr�ation de la ligne de titre
        Row row = sheet.createRow(rowCount++);
        Cell cell = row.createCell(0); cell.setCellValue("Url"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(1); cell.setCellValue("Firstname"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(2); cell.setCellValue("Lastname"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(3); cell.setCellValue("Title"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(4); cell.setCellValue("Company"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(5); cell.setCellValue("Age"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(6); cell.setCellValue("Location"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(7); cell.setCellValue("Status"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(8); cell.setCellValue("Availability"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(9); cell.setCellValue("Email"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(10); cell.setCellValue("Phone"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(11); cell.setCellValue("Mobile"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(12); cell.setCellValue("Skype"); cell.setCellStyle(styles.get("title"));
        cell = row.createCell(13); cell.setCellValue("Linkedin"); cell.setCellStyle(styles.get("title"));
                
        //cr�ation des lignes de contacts
        for (ContactDoYouBuzz contact : contactsDoYouBuzz) {
        	
        	row = sheet.createRow(rowCount++);
        	
        	cell = row.createCell(0); cell.setCellValue(contact.getUrl()); cell.setCellStyle(styles.get("normal"));
            cell = row.createCell(1); cell.setCellValue(contact.getFirstname()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(2); cell.setCellValue(contact.getLastname()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(3); cell.setCellValue(contact.getTitle()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(4); cell.setCellValue(contact.getCompany()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(5); cell.setCellValue(contact.getAge()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(6); cell.setCellValue(contact.getLocation()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(7); cell.setCellValue(contact.getStatus()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(8); cell.setCellValue(contact.getAvailability()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(9); cell.setCellValue(contact.getEmail()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(10); cell.setCellValue(contact.getPhone()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(11); cell.setCellValue(contact.getMobile()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(12); cell.setCellValue(contact.getSkype()); cell.setCellStyle(styles.get("normal"));
    	    cell = row.createCell(13); cell.setCellValue(contact.getLinkedin()); cell.setCellStyle(styles.get("normal"));
    	 
        }
        
        //taille des colonnes (the width is measured in units of 1/256th of a character width)
        sheet.setColumnWidth(0, 256*60);
        sheet.setColumnWidth(1, 256*30);
        sheet.setColumnWidth(2, 256*30);
        sheet.setColumnWidth(3, 256*30);
        sheet.setColumnWidth(4, 256*30);
        sheet.setColumnWidth(5, 256*20);
        sheet.setColumnWidth(6, 256*30);
        sheet.setColumnWidth(7, 256*20);
        sheet.setColumnWidth(8, 256*30);
        sheet.setColumnWidth(9, 256*30);
        sheet.setColumnWidth(10, 256*30);
        sheet.setColumnWidth(11, 256*30);
        sheet.setColumnWidth(12, 256*30);
        sheet.setColumnWidth(13, 256*70);
        sheet.setZoom(80);
        
        //�criture du fichier
        try (FileOutputStream outputStream = new FileOutputStream(ResultPath + "Extract-DoYouBuzz-" + KeywordsInTitle + "-" + Keywords + ".xlsx")) {
            workbook.write(outputStream);
        } 
        catch (IOException e) { e.printStackTrace(); }
        
        System.out.println("	Excel cr��");
        
	}
	
	 public static Map<String, CellStyle> createStyles(Workbook wb) {
		 
	        Map<String, CellStyle> styles = new HashMap<>();

	        BorderStyle thin = BorderStyle.THIN;
	        short borderColor = IndexedColors.BLACK.getIndex();

	        CellStyle titleStyle;
	        Font font = wb.createFont();
	        font.setBold(true);
	        titleStyle = wb.createCellStyle();
	        titleStyle.setFont(font);
	        titleStyle.setAlignment(HorizontalAlignment.CENTER);
	        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	        titleStyle.setWrapText(true);
	        titleStyle.setBottomBorderColor(borderColor);
	        titleStyle.setLeftBorderColor(borderColor);
	        titleStyle.setRightBorderColor(borderColor);
	        titleStyle.setTopBorderColor(borderColor);
	        titleStyle.setBorderBottom(thin);
	        titleStyle.setBorderLeft(thin);
	        titleStyle.setBorderRight(thin);
	        titleStyle.setBorderTop(thin);
	        styles.put("title", titleStyle);
	        
	        CellStyle normalStyle;
	        normalStyle = wb.createCellStyle();
	        normalStyle.setAlignment(HorizontalAlignment.CENTER);
	        normalStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	        normalStyle.setWrapText(true);
	        normalStyle.setBottomBorderColor(borderColor);
	        normalStyle.setLeftBorderColor(borderColor);
	        normalStyle.setRightBorderColor(borderColor);
	        normalStyle.setTopBorderColor(borderColor);
	        normalStyle.setBorderBottom(thin);
	        normalStyle.setBorderLeft(thin);
	        normalStyle.setBorderRight(thin);
	        normalStyle.setBorderTop(thin);
	        styles.put("normal", normalStyle);

	        return styles;
	    }
}
